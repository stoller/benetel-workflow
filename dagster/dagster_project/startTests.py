import traceback
import os
import re
import time
import json
import statistics
from datetime import datetime
from typing import List, NoReturn
from tabulate import tabulate

from dagster import op, graph, job, fs_io_manager, Out, Output
from dagster import DagsterError, DagsterExecutionInterruptedError
from dagster import execute_job, reconstructable
from dagster import Failure, failure_hook, HookContext, get_dagster_logger
from dagster_shell import execute_shell_command

from .params import input_params
from .resources import resource_defs
from . import workflowCreds

ATTENUATION_CONSTANT = 76

# We want to pass the blob into the hook from every failure
def raiseFailure(status, description) -> NoReturn:
    raise Failure(metadata={"status" : status}, description=description)

def updateExecution(context, params, execution, result, comments=None):
    if params["params"]["skip_tcms"]:
        return
    
    kiwiTCMS = context.resources.kiwiTCMS
    kiwiTCMS.updateExecution(params, execution, result, comments=comments)
    pass

def addAttachment(context, params, filename, content):
    if params["params"]["skip_tcms"]:
        return
    
    kiwiTCMS = context.resources.kiwiTCMS
    kiwiTCMS.addAttachment(params, filename, content)
    pass

#
# I want to save all the log info in Kiwi and this is, by far, the
# easiest way to accomplish this.
#
def logInfo(context, params, message):
    context.log.info(message)
    now = datetime.now()
    t = now.strftime('%I:%M:%S %p: ')
    
    params["log"] += t + message + "\n"
    return

def clearLog(context, params):
    params["log"] = ""
    return

def saveLog(context, params):
    addAttachment(context, params, "dagster-log", params["log"])
    return

#
# start() drags stuff out of the incoming params to pass along.
#
@op(config_schema={"params" : dict},
    required_resource_keys={"kiwiTCMS"},
    out={"params": Out()})
def start(context) -> dict:
    params  = context.op_config["params"]
    domain  = "." + params["sut_experiment_name"] + "." + params["proj"]
    #
    # This gets carried along
    #
    status =  {
        "ue"         : "ue" + domain,
        "cudu"       : "cudu" + domain,
        "cn5g"       : "cn5g" + domain,
        "server"     : "10.45.0.1",
        "params"     : params,
        "kiwidata"   : {},
        "log"        : "",
    }
    if params["skip_tcms"]:
        context.log.info("TCMS/KIWI is disabled")
    else:
        context.log.info("TCMS/KIWI is enabled")
        kiwiTCMS = context.resources.kiwiTCMS
        kiwiTCMS.setup(status)
        # Debugging
        if False:
            kiwiTCMS.createTestRun(status, "Tester")
            attach = kiwiTCMS.addTestCase(status, "Attach")
            kiwiTCMS.updateExecution(status, attach, "PASSED", comments=["Bla"])
            detach = kiwiTCMS.addTestCase(status, "Detach")
            kiwiTCMS.updateExecution(status, Detach, "PASSED", comments=["Alb"])
            pass
        pass
    context.log.debug(str(status))
    return status

#
# Start the AP experiment if not running.
#
@op(required_resource_keys={"portal"},
    out={"start": Out(is_required=False, dagster_type=bool),
         "started": Out(is_required=False, dagster_type=bool)})   
def CheckSutStarted(context, params: dict):
    #context.log.info(str(status))
    #params = context.op_config["params"]
    portal = context.resources.portal
    #context.log.info(str(portal))

    result = portal.run(action="experiment.status",
                        name=params["params"]["sut_experiment_name"],
                        proj=params["params"]["proj"],
                        max_wait_time=5, errors_are_fatal=False,
                        wait_for_status=False,
                        context=context)

    # Return False if the experiment is allready running
    if result["result_code"] != 0:
        yield Output(True, "start")
    else:
        yield Output(False, "started")
        pass
    pass

@op(required_resource_keys={"portal"})
def StartSut(context, params: dict, start: bool) -> bool:
    #params = context.op_config["params"]
    portal = context.resources.portal

    #
    # Start the experiment and wait. 
    #
    result = portal.run(action="experiment.create",
                        name=params["params"]["sut_experiment_name"],
                        proj=params["params"]["proj"],
                        profile=params["params"]["sut_profile_name"],
                        paramset=params["params"]["sut_paramset"],
                        sshpubkey=workflowCreds.portalCreds["sshpubkey"],
                        max_wait_time=3600,
                        maxduration=1,
                        # Polling Interval, does not belong here.
                        interval=30,
                        errors_are_fatal=True,
                        wait_for_status=True,
                        wait_for_execute_status=True,
                        context=context)
    context.log.debug(str(result))

    if result["result_code"] != 0:
        #
        # Either a start error or a status error. If the later then the experiment
        # still exists and probably needs to be terminated. Need to think about that.
        #
        if "status" in result:
            description = result["status"].get("failure_message","unknown")
            pass
        else:
            description = result["error"]
            pass
        raise Failure(description=description)
    
    return True

@op
def SutStarted(context, started: bool) -> bool:
    return True

@op
def SutReady(context, started: list) -> bool:
    assert started == [True] or started == [False]
    return started == [True] or started == [False]

#
# Multiple condition (attenuation) tests.
#
def createConditions(params, bidir=False):
    def createResults():
        if bidir:
            return {
                "upload"   : {},
                "download" : {},
            }
        return {}

    conditions   = [
        {
            "description" : "Excellent",
            "attenuation" : 0,
            "results"     : createResults(),
        },
    ]
    # Debugging
    if False:
        conditions.extend([
            {
                "description" : "Good",
                "attenuation" : 10,
                "results"     : createResults(),
            },
            {
                "description" : "Fair",
                "attenuation" : 20,
                "results"     : createResults(),
            },
            {
                "description" : "Poor",
                "attenuation" : 30,
                "results"     : createResults(),
            },
        ])
        pass
    return conditions

#
# Utility routine, needs to move
#
def runRemoteCommand(context, params, node, command, timeout=45):
    sshClient = context.resources.sshClient

    try:
        index = sshClient.runRemoteCommand(params["params"]["username"],
                                           node, command, timeout=timeout);
    except:
        context.log.info(traceback.format_exc())
        raiseFailure(params, "Command failed (connection): " + command)
        pass

    if sshClient.getRemoteExitStatus(index):
        raiseFailure(params, "Command failed (status): " + command)
        pass

    results = sshClient.getRemoteResults(index)
    return results

#
# Start but do not wait. 
#
def startRemoteCommand(context, params, node, command):
    sshClient = context.resources.sshClient

    try:
        index = sshClient.runRemoteCommand(params["params"]["username"],
                                           node, command);
    except:
        context.log.info(traceback.format_exc())
        raiseFailure(params, "Command failed (connection): " + command)
        pass

    return index;

#
# Stop a command we started above, reading the data
#
def stopRemoteCommand(context, params, index):
    sshClient = context.resources.sshClient

    results = sshClient.stopRemoteCommand(index)
    return results

#
# Restart the GNB before starting a set of tests. For one, this will
# truncate the GNB log file to a manageable size. 
#
def restartGNB(context, params):
    stop    = "sudo systemctl stop srs-gnb"
    start   = "sudo systemctl start srs-gnb"

    context.log.info("Stopping CU/DU")
    runRemoteCommand(context, params, params["cudu"], stop)
    logInfo(context, params, "Starting CU/DU")
    runRemoteCommand(context, params, params["cudu"], start)
    time.sleep(1)
    return

#
# As per discussion with Dustin, we want to make sure that we have a consistent
# initial state, with first attach to ru1 every time. So drive the attenuation
# to ru1 before we attach,
#
def attachUE(context, params, ru, execution=None):
    attach = "sudo /var/tmp/quectel_control.py up"

    logInfo(context, params, "Attaching UE")
    runRemoteCommand(context, params, params["ue"], attach)
    logInfo(context, params, "Waiting a moment")
    time.sleep(3)
    logInfo(context, params, "Running a ping to confirm attach")

    #
    # We want to catch this failure and abort
    #
    pingResults = ""
    try:
        pingResults = runPing(context, params, count=5)
        status = "PASSED"
    except DagsterExecutionInterruptedError:
        raise
    except:
        status = "FAILED"
        pass

    if execution:
        updateExecution(context, params, execution, status,
                        comments=[pingResults])
        pass

    return status == "PASSED"

#
# Detach and run a ping test to confirm
#
def detachUE(context, params, execution=None):
    command = "sudo /var/tmp/quectel_control.py down"

    logInfo(context, params, "Detaching UE")
    runRemoteCommand(context, params, params["ue"], command)
    logInfo(context, params, "Waiting a moment")
    time.sleep(2)
    logInfo(context, params, "Running a ping to confirm detach")
    #
    # We want this to fail, but catch it cause that is good.
    #
    pingResults = ""
    try:
        pingResults = runPing(context, params, count=5)
        status = "FAILED"
    except DagsterExecutionInterruptedError:
        raise
    except:
        status = "PASSED"
        pass

    if execution:
        updateExecution(context, params, execution, status,
                        comments=[pingResults])
        pass
    return

#
# Get the GNB log which is very big.
#
def getGNBLog(context, params):
    command = "/usr/bin/cat /var/log/gnb.log"

    results = runRemoteCommand(context, params, params["cudu"], command);
    return results

#
# Get the ue-metrics file.
#
def getUEMetrics(context, params):
    command = "/usr/bin/cat /var/log/ue-metrics.log"

    results = runRemoteCommand(context, params, params["ue"], command);
    return results

#
# Get the gnb TX/RX metrics.
#
def getGNBTXRXMetrics(context, params):
    command  = "grep -e 'Handover' -e 'TX metrics' -e 'RX metrics' "
    command += "/var/log/gnb.log"

    results = runRemoteCommand(context, params, params["cudu"], command);
    return results

#
# Get the gnb-metrics file.
#
def getGNBMetrics(context, params):
    command = "/usr/bin/cat /var/log/gnb-metrics.log"

    results = runRemoteCommand(context, params, params["cudu"], command);
    return results

#
# Bracket tests by starting/stopping the metrics gathering services.
# This gives us clean log files of metrics
#
def startMetricsGathering(context, params):
    ue = params["ue"]
    cudu = params["cudu"]

    command = "sudo systemctl restart ue-metrics"
    logInfo(context, params, "Starting UE metrics gathering service")
    runRemoteCommand(context, params, ue, command)

    command = "sudo systemctl restart srs-gnb-metrics"
    logInfo(context, params, "Starting CU/DU metrics gathering service")
    runRemoteCommand(context, params, cudu, command)
    pass

def stopMetricsGathering(context, params):
    ue = params["ue"]
    cudu = params["cudu"]

    command = "sudo systemctl stop srs-gnb-metrics"
    logInfo(context, params, "Stopping CU/DU metrics gathering service")
    runRemoteCommand(context, params, cudu, command)

    command = "sudo systemctl stop ue-metrics"
    logInfo(context, params, "Stopping UE metrics gathering service")
    runRemoteCommand(context, params, ue, command)
    pass

def getMetrics(context, params):
    logInfo(context, params, "Getting UE metrics")
    ue_metrics = getUEMetrics(context, params)
    
    logInfo(context, params, "Getting CU/DU metrics")
    gnb_metrics = getGNBMetrics(context, params)
    
    return [ue_metrics, gnb_metrics]

def parseUEMetrics(ue_metrics, results):
    for line in ue_metrics.splitlines():
        stuff = json.loads(line)
        for key in ["RSRP", "RSRQ", "SINR"]:
            if key in stuff:
                val = float(stuff[key])
                key = "UE " + key
                if not key in results:
                    results[key] = []
                    pass
                results[key].append(val)
                pass
            pass
        pass
    pass

def parseGNBMetrics(gnb_metrics, results):
    for line in gnb_metrics.splitlines():
        try:
            stuff = json.loads(line)
        except Exception as exc:
            continue
                
        if "ue_list" in stuff and len(stuff["ue_list"]):
            ue_dict = stuff["ue_list"][0]["ue_container"]
            for key,val in ue_dict.items():
                if key == "cqi":
                    key = "UE CQI"
                elif key == "dl_mcs":
                    key = "PDSCH MCS"
                elif key == "ri":
                    key = "MIMO rank"
                elif key == "dl_brate":
                    key = "L1 DL throughput (Mbps)"
                    val = val / (1000 * 1000)
                elif key == "ul_brate":
                    key = "L1 UL throughput (Mbps)"
                    val = val / (1000 * 1000)
                elif key == "bsr":
                    key = "UE Buffer status"
                elif key == "dl_nof_nok":
                    key = "PDSCH BLER [%]"
                    if val != 0:
                        val = val / (ue_dict["dl_nof_ok"] + val)
                        pass
                    pass
                else:
                    continue

                if not key in results:
                    results[key] = []
                    pass
                results[key].append(val)
                pass
            pass
        pass
    pass        

#
# Get registration/deregistration timestamps. This is a little cumbersome,
# we do not want to tail the file, we just want the last two lines that
# are relevant. 
#
def getRegDeregTime(context, params, which):
    command = "sudo journalctl -u open5gs-amfd --since -1m | grep "
    if which == "registration":
        command += "-e 'Registration request' -e 'Registration complete'"
    else:
        command += "-e 'Deregistration request' -e 'Number of gNB-UEs is now 0'"
        pass
    command += " | tail -n 2"
    results = runRemoteCommand(context, params, params["cn5g"], command);

    #
    # Convert the timestamps into a delta (milliseconds)
    #
    r,d = results.splitlines()
    rm = re.search("\d\d\/\d\d (\d\d:\d\d:\d\d\.\d+):", r)
    dm = re.search("\d\d\/\d\d (\d\d:\d\d:\d\d\.\d+):", d)
    if not rm or not dm:
        raiseFailure(params, "Bad reg/dereg lines")
        pass
    md = datetime.strptime(rm.group(1), "%H:%M:%S.%f")
    dd = datetime.strptime(dm.group(1), "%H:%M:%S.%f")
    if not rm or not dd:
        raiseFailure(params, "Bad reg/dereg timestamps")
        pass
    delta = (dd - md)
    return [results, delta.total_seconds() * 1000]

#
# Set the attenuation for an RU.
#
def setAttenuation(context, params, ru, attenuation=0):
    command = "/local/repository/bin/update-attens %s %d" % (ru, attenuation)
    
    logInfo(context, params,
            "Setting attenuation for ru %s to %d" % (ru, attenuation))
    results = runRemoteCommand(context, params, params["cudu"], command)
    return

#
# Initiate Handover
#
def initiateHandover(context, params, ru, timeout=10):
    sshClient = context.resources.sshClient
    command = "/local/repository/bin/handover " + ru
    iperf  = "iperf3 -V -p 5000 -t " + str(timeout) + " "
    iperf += "--bidir -u -b80M -c " + params["server"]

    #
    # The iperf runs in the background during the handover.
    #
    logInfo(context, params, "Starting up background bidirectional iperf")
    index = startRemoteCommand(context, params, params["ue"], iperf)
    logInfo(context, params, "Changing attenuation to trigger handover")
    results = runRemoteCommand(context, params, params["cudu"], command)
    logInfo(context, params, "Waiting for a little while to settle")
    time.sleep(3)
    iperf_results = stopRemoteCommand(context, params, index)
    return iperf_results

#
# Run a ping test
#
def runPing(context, params, count=10):
    command = "ping -c " + str(count) + " -i 0.5 " + params["server"]
    #logInfo(context, params, command)

    results = runRemoteCommand(context, params, params["ue"], command)
    if params["params"]["condensed"]:
        logInfo(context, params, "\n".join(results.splitlines()[-2:-1:]))
    else:
        logInfo(context, params, results)
        pass
    return results

#
# Run an iperf test
#
def runIperf(context, params, timeout=5, direction="bidir", proto="UDP"):
    command  = "iperf3 -V -p 5000 --format m -t " + str(timeout) + " "
    if proto == "UDP":
        command += "-u -b 80M -l 1300 "
        pass
    if direction == "bidir":
        command += "--bidir "
    elif direction == "download":
        command += "-R "
        pass
    command += "-c " + params["server"]
    logInfo(context, params, command)

    results = runRemoteCommand(context, params, params["ue"], command,
                               timeout=timeout+15)
    return results

#
# Dig out summary results
#
def parseIperfResults(iperf_results, results, direction):
    insum = False
    for line in iperf_results.splitlines():
        if insum == False:
            if "Summary Results" in line:
                insum = True
                pass
            continue
        match = re.search("([\d\.]+)\s+Mbits/sec(.*)receiver$", line)
        if match:
            mbits = float(match.group(1))
            if direction == "upload":
                key = "Application UL throughput (Mbps)"
            else:
                key = "Application DL throughput (Mbps)"
                pass
            
            if not key in results:
                results[key] = []
                pass
            results[key].append(mbits)
            pass
        pass
    pass

#
# Browser test using curl
#
def runCurl(context, params):
    command  = "/bin/curl --output /dev/null -w "
    command += "'DNS:%{time_namelookup}\nTTFB:%{time_starttransfer}\n"
    command += "Throughput:%{speed_download}\nPageLoadTime:%{time_total}\n' " 
    command += "http://10.45.0.1/random-data"

    results = runRemoteCommand(context, params, params["ue"], command)
    return results

#
# Dig out -w metrics
#
def parseCurlResults(curl_results):
    results = { }
    
    for line in curl_results.splitlines():
        if ("DNS:" in line or
            "TTFB:" in line or
            "Throughput:" in line or
            "PageLoadTime:" in line):
            m = re.search("^(\w+):([\d\.]+)", line)
            if not m:
                raiseFailure(params, "Invalid curl metric: " + line)
                pass
            key = m.group(1)
            val = float(m.group(2))

            if key == "DNS" or key == "TTFB":
                val = val * 1000
                key = key + " (ms)"
            elif key == "Throughput":
                val = (val * 8) / (1000 * 1000)
            elif key == "PageLoadTime":
                key == "Page Load Time (sec)"
                pass
            
            results[key] = val
            pass
        pass
    return results

#
# FTP test using curl
#
def runftp(context, params, direction):
    url   = "ftp://totaman:randopass@10.45.0.1/"
    fname = "random-data"
    command  = "/bin/curl --output /dev/null "
    metrics  = "DNS:%{time_namelookup}\nTTFB:%{time_starttransfer}\n"
    metrics += "PageLoadTime:%{time_total}\n"

    if direction == "upload":
        metrics += "Throughput:%{speed_upload}\n"
        command += "-w '" + metrics + "' "
        # Delete file from server, for next iteration. Add file to upload
        command += "-Q 'DELE " + fname + "' "
        command += "-T /tmp/" + fname
    else:
        metrics += "Throughput:%{speed_download}\n"
        command += "-w '" + metrics + "' "
        pass
    
    command += " " + url
    # Add file to download
    if direction == "download":
        command += fname
        pass
    
    results = runRemoteCommand(context, params, params["ue"], command)
    return results

#
# Stub to join the conditional
#
@op(required_resource_keys={"sshClient", "kiwiTCMS"})
def initTests(context, params: dict):
    if False:
        raiseFailure(params, "Aborting")
        pass
    
    #
    # Force detach
    #
    detach = "sudo /var/tmp/quectel_control.py down"
    logInfo(context, params, "Making sure UE is detached")
    runRemoteCommand(context, params, params["ue"], detach)
    time.sleep(1)

    #
    # Drive the attenuation so that we always start at ru1
    #
    switch  = "/local/repository/bin/handover bru1"
    logInfo(context, params, "Forcing attenuation to bru1")
    runRemoteCommand(context, params, params["cudu"], switch)

    restartGNB(context, params)
    return params

#
# Cleanup
#
@op(required_resource_keys={"sshClient", "kiwiTCMS"})
def cleanup(context, params: dict):
    stop = "sudo systemctl stop srs-gnb"

    logInfo(context, params, "Stopping CU/DU")
    runRemoteCommand(context, params, params["cudu"], stop)
    return params

#
# Run Attach/Detach test. TIFG section 4.3 (registration and deregistration)
#
@op(required_resource_keys={"sshClient", "kiwiTCMS"})
def testRegistration(context, params: dict):
    kiwiTCMS  = context.resources.kiwiTCMS
    sshClient = context.resources.sshClient
    ue = params["ue"]
    timeStamps = {
        "registration"   : [],
        "deregistration" : [],
    }
    iterations = 10
    iperftime  = 30
    # Speed things along
    if params["params"]["debug"]:
        iterations = 3
        iperftime  = 5
        pass

    if False:
        return params

    clearLog(context, params)

    # Create TestRun and associated TestCases.
    kiwiTCMS.createTestRun(params,
                           "5G SA registration and deregistration of single UE "+
                           "(TIFG E2E 4.3)")

    count = 0
    while iterations:
        it_text  = "(iteration " + str(count + 1) + ")";
        attachTE = kiwiTCMS.addTestCase(params, "Registration " + it_text)
        iperfTE  = kiwiTCMS.addTestCase(params, "Throughput " + it_text)
        detachTE = kiwiTCMS.addTestCase(params, "Deregistration " + it_text)

        logInfo(context, params, "testAttachDetach " + it_text)

        #
        # Start gathering metrics. 
        #
        metrics = "sudo systemctl restart ue-metrics"
        logInfo(context, params, "Starting metrics gathering service")
        runRemoteCommand(context, params, ue, metrics)

        #
        # Attach
        #
        attach = "sudo /var/tmp/quectel_control.py up"
        logInfo(context, params, "Attaching UE")
        runRemoteCommand(context, params, ue, attach)
        logInfo(context, params, "Waiting a moment")
        time.sleep(3)

        #
        # Get the registration times
        #
        logInfo(context, params, "Getting registration time stamps")
        regInfo,regTime = getRegDeregTime(context, params, "registration")
        regInfo += "\n" + "Registration time: " + str(regTime) + " ms\n"
        logInfo(context, params, regInfo);

        #
        # Confirm with a ping.
        # We want to catch this failure and abort
        #
        logInfo(context, params, "Confirming attach with ping")
        pingResults = ""
        try:
            pingResults = runPing(context, params, count=5)
            status = "PASSED"
        except DagsterExecutionInterruptedError:
            raise
        except Exception as exc:
            context.log.debug(str(exc))
            status = "FAILED"
            pass

        updateExecution(context, params, attachTE, status,
                        comments=[regInfo, pingResults])
        if status == "FAILED":
            raiseFailure(params, "Failed to attach, aborting")
            pass
    
        #
        # Throughput test with iperf
        #
        logInfo(context, params, "Running iperf, this will take a little while")
        iperfResults = runIperf(context, params, timeout=iperftime)
        addAttachment(context, params, "throughput " + it_text , iperfResults)

        #
        # Break out the last lines (summary) for a comment
        #
        iperf_summary = None
        for line in iperfResults.splitlines():
            if "Summary Results" in line:
                iperf_summary = ""
                pass
            if iperf_summary != None:
                iperf_summary += line + "\n"
                pass
            pass
        if params["params"]["condensed"]:
            logInfo(context, params, "iperf complete")
        else:
            logInfo(context, params,iperf_summary)
            pass
        updateExecution(context, params, iperfTE, "PASSED", comments=[iperf_summary])

        #
        # Detach
        #
        detach = "sudo /var/tmp/quectel_control.py down"
        logInfo(context, params, "Detaching UE")
        runRemoteCommand(context, params, ue, detach)
        logInfo(context, params, "Waiting a moment")
        time.sleep(3)

        #
        # Get the registration times
        #
        logInfo(context, params, "Getting deregistration time stamps")
        deregInfo,deregTime = getRegDeregTime(context, params, "deregistration")
        deregInfo += "\n" + "Deregistration time: " + str(deregTime) + " ms\n"
        logInfo(context, params, deregInfo);

        #
        # Confirm the detach.
        #
        logInfo(context, params, "Running a ping to confirm detach")
        #
        # We want this to fail, but catch it cause that is good.
        #
        pingResults = ""
        try:
            pingResults = runPing(context, params, count=5)
            status = "FAILED"
        except DagsterExecutionInterruptedError:
            raise
        except:
            status = "PASSED"
            pass
        updateExecution(context, params, detachTE, status,
                        comments=[deregInfo, pingResults])

        #
        # Grab the metrics file and attach to the run.
        #
        logInfo(context, params, "Getting UE metrics")
        metrics = getUEMetrics(context, params)
        addAttachment(context, params, "ue-metrics " + it_text, metrics)

        timeStamps["registration"].append(regTime)
        timeStamps["deregistration"].append(deregTime)
        
        iterations = iterations - 1
        count = count + 1
        pass

    logInfo(context, params, str(timeStamps))

    minReg    = "%.2f" % min(timeStamps["registration"])
    maxReg    = "%.2f" % max(timeStamps["registration"])
    meanReg   = "%.2f" % statistics.mean(timeStamps["registration"])
    minDereg  = "%.2f" % min(timeStamps["deregistration"])
    maxDereg  = "%.2f" % max(timeStamps["deregistration"])
    meanDereg = "%.2f" % statistics.mean(timeStamps["deregistration"])

    #
    # Use python tabulate module to format a table.
    #
    header = ["KPI"]
    row1    = ["Registration (ms)"]
    row2    = ["Deregistration (ms)"]

    # Init header
    count = len(timeStamps["registration"])
    # Column for each iteration
    for x in range(count):
        header.append(x + 1)
        pass
    # Summary columns
    header.append("Min")
    header.append("Max")
    header.append("Avg")

    # Two rows.
    for t in timeStamps["registration"]:
        row1.append("%.2f" % t)
        pass
    row1.extend([minReg, maxReg, meanReg])
    
    for t in timeStamps["deregistration"]:
        row2.append("%.2f" % t)
        pass
    row2.extend([minDereg, maxDereg, meanDereg])
    
    table = tabulate([row1, row2], header, tablefmt="grid")
    
    logInfo(context, params, table)
    addAttachment(context, params, "Summary Results", table)
    saveLog(context, params);
    return params
    
#
# Run Handover test. TIFG section 4.4 (Intra-O-DU mobility)
#
@op(required_resource_keys={"sshClient", "kiwiTCMS"})
def testHandover(context, params: dict):
    kiwiTCMS  = context.resources.kiwiTCMS
    sshClient = context.resources.sshClient
    ue = params["ue"]

    if False:
        return params

    clearLog(context, params)

    # Create TestRun and associated TestCases.
    kiwiTCMS.createTestRun(params, "Inter-O-DU mobility (TIFG E2E 4.5)")
    attachTE   = kiwiTCMS.addTestCase(params, "Registration")
    handoverTE = kiwiTCMS.addTestCase(params, "Handover")
    detachTE   = kiwiTCMS.addTestCase(params, "Deregistration")

    # Reset the GNB logs
    restartGNB(context, params)
    
    #
    # Start gathering metrics. 
    #
    metrics = "sudo systemctl restart ue-metrics"
    logInfo(context, params, "Starting metrics gathering service")
    runRemoteCommand(context, params, ue, metrics)

    # No metrics needed.
    attachUE(context, params, "bru1", attachTE)

    logInfo(context, params, "Attempting handover to bru2")
    iperf_results = initiateHandover(context, params, "bru2", timeout=10)
    addAttachment(context, params, "throughput", iperf_results)

    logInfo(context, params, "Grabbing the CU/DU TX/RX metrics")
    gnbmetrics = getGNBTXRXMetrics(context, params)

    status = "WAIVED"
    handover_info = ""
    txrx_metrics  = ""
        
    for line in gnbmetrics.splitlines():
        if "Handover" in line:
            logInfo(context, params, line)
            handover_info += line + "\n"
        
            if "failed" in line:
                status = "FAILED"
                pass
            if "finalized" in line:
                status = "PASSED"
                pass
            continue
        
        if "TX metrics" in line or "RX metrics" in line:
            #logInfo(context, params, line)
            txrx_metrics += line + "\n"
            continue
        pass

    if txrx_metrics != "":
        addAttachment(context, params, "txrx-metrics", txrx_metrics)
        pass

    #
    # Break out the last lines (summary) for a comment
    #
    iperf_summary = None
    for line in iperf_results.splitlines():
        if "Summary Results" in line:
            iperf_summary = ""
            pass
        if iperf_summary != None:
            iperf_summary += line + "\n"
            pass
        pass
    if not params["params"]["condensed"]:
        logInfo(context, params, iperf_summary)
        pass

    # WAIVED will mean no handover was found (so not attempted?)
    updateExecution(context, params, handoverTE, status,
                    comments=[handover_info, iperf_summary])

    #
    # Detach and run a ping test to confirm.
    #
    detachUE(context, params, detachTE)

    #
    # Grab the metrics file and attach to the run.
    #
    logInfo(context, params, "Getting UE metrics")
    metrics = getUEMetrics(context, params)
    addAttachment(context, params, "ue-metrics", metrics)

    #
    # Drive the attenuation back to bru1
    #
    switch  = "/local/repository/bin/handover bru1"
    logInfo(context, params, "Forcing attenuation back to bru1")
    runRemoteCommand(context, params, params["cudu"], switch)

    saveLog(context, params)
    return params

#
# Run web browsing test. TIFG section 6.1.1.1
#
@op(required_resource_keys={"sshClient", "kiwiTCMS"})
def testWebBrowsing(context, params: dict):
    kiwiTCMS  = context.resources.kiwiTCMS
    sshClient = context.resources.sshClient
    ue = params["ue"]
    cudu = params["cudu"]
    conditions = createConditions(params)

    if False:
        return params

    clearLog(context, params)

    # Create TestRun and associated TestCases.
    kiwiTCMS.createTestRun(params, "Web Browsing "+ "(TIFG E2E 6.1.1)")

    for condition in conditions:
        description = condition["description"]
        attenuation = condition["attenuation"]
        iterations  = 6
        # Speed things along
        if params["params"]["debug"]:
            iterations = 2
            pass

        setAttenuation(context, params, "bru1", attenuation)

        count = 0
        while iterations:
            it_text  = "(condition %s, iteration %d)" % (description, count+1)
            attachTE = kiwiTCMS.addTestCase(params, "Registration " + it_text)
            browseTE = kiwiTCMS.addTestCase(params, "Browse File " + it_text)
            detachTE = kiwiTCMS.addTestCase(params, "Deregistration " + it_text)

            logInfo(context, params, "testWebBrowsing " + it_text)

            # Reset the GNB logs
            restartGNB(context, params)
    
            attachUE(context, params, "bru1", attachTE)

            # Start gathering metrics.
            startMetricsGathering(context, params)

            logInfo(context, params, "Running curl")
            curl_results = runCurl(context, params)
            if params["params"]["condensed"]:
                logInfo(context, params, "curl complete")
            else:
                logInfo(context, params, curl_results)
                pass
            updateExecution(context, params, browseTE, "PASSED",
                            comments=[curl_results])

            # Stop the metrics gathering so the log files stop getting data
            stopMetricsGathering(context, params)

            # Grab the metrics and attach to the run.
            ue_metrics,gnb_metrics = getMetrics(context, params)
            addAttachment(context, params, "ue-metrics " + it_text, ue_metrics)
            addAttachment(context, params, "cudu-metrics " + it_text, gnb_metrics)

            detachUE(context, params, detachTE)

            #
            # Dig out stats
            #
            results = condition["results"]

            # The curl output includes -w metrics.
            curl_metrics = parseCurlResults(curl_results)

            for key,val in curl_metrics.items():
                # Better name for this metric
                if key == "Throughput":
                    key = "App DL throughput (Mbps)"
                    pass

                if not key in results:
                    results[key] = []
                    pass
                results[key].append(val)
                pass

            # Add some of the UE metrics that are required.
            parseUEMetrics(ue_metrics, results)
            
            # Add some GNB metrics.
            parseGNBMetrics(gnb_metrics, results)
            
            iterations = iterations - 1
            count = count + 1
            pass
        #
        # Compute mean and std deviation for each
        #
        stats = {}
        for key,values in condition["results"].items():
            stats[key] = {
                "mean"  : statistics.mean(values),
                "stdev" : statistics.stdev(values),
            }
            pass

        condition["stats"] = stats
        pass

    pp_conditions = json.dumps(conditions, indent=4)
    if not params["params"]["condensed"]:
        logInfo(context, params, pp_conditions)
        pass
    addAttachment(context, params, "json-summary", pp_conditions)

    # Do not leave at non-zero
    setAttenuation(context, params, "bru1", 0)

    #
    # Use python tabulate module to format a table.
    #
    header = ["Test"]
    rows    = []

    # Init header and tests
    for condition in conditions:
        description = condition["description"]
        attenuation = condition["attenuation"]
        header.append("%s\nAtten: %d dB (%d)"%
                      (description, attenuation + ATTENUATION_CONSTANT,
                       attenuation))
        pass
    for key,val in conditions[0]["results"].items():
        rows.append([key + "\n" + "mean/stdev"])
        pass

    for condition in conditions:
        index = 0
        for key,stats in condition["stats"].items():
            if key.startswith("DNS"):
                rows[index].append("N/A")
            else:
                rows[index].append("%.4f\n%.4f" % (stats["mean"], stats["stdev"]))
                pass
            index = index + 1
            pass
        pass

    table = tabulate(rows, header, tablefmt="grid")
    if not params["params"]["condensed"]:
        logInfo(context, params, table)
        pass
    addAttachment(context, params, "Summary Results", table)
    
    saveLog(context, params);
    return params

#
# 
#
@op(required_resource_keys={"sshClient", "kiwiTCMS"})
def testFTP(context, params: dict):
    kiwiTCMS  = context.resources.kiwiTCMS
    sshClient = context.resources.sshClient
    ue = params["ue"]
    cudu = params["cudu"]
    conditions = createConditions(params, bidir=True)

    if False:
        return params

    clearLog(context, params)

    # Need to create the file of random data that is uploaded.
    os.system("dd if=/dev/random of=/tmp/random-data bs=64k count=200")

    # Create TestRun and associated TestCases.
    kiwiTCMS.createTestRun(params, "FTP Test "+ "(TIFG E2E 6.1.2)")

    for condition in conditions:
        description = condition["description"]
        attenuation = condition["attenuation"]
        iterations  = 6
        # Speed things along
        if params["params"]["debug"]:
            iterations = 2
            pass

        setAttenuation(context, params, "bru1", attenuation)

        count = 0
        while iterations:
            it_text  = "(condition %s, iteration %d)" % (description, count+1)
            attachTE   = kiwiTCMS.addTestCase(params, "Registration " + it_text)
            uploadTE   = kiwiTCMS.addTestCase(params, "Upload File " + it_text)
            downloadTE = kiwiTCMS.addTestCase(params, "Download File " + it_text)
            detachTE   = kiwiTCMS.addTestCase(params, "Deregistration " + it_text)

            logInfo(context, params, "testFTP " + it_text)

            # Reset the GNB logs
            restartGNB(context, params)
    
            attachUE(context, params, "bru1", attachTE)

            for test in ["upload", "download"]:
                logInfo(context, params, "Running " + test + " test")

                # Start gathering metrics.
                startMetricsGathering(context, params)

                logInfo(context, params, "Running ftp " + test)
                ftp_results = runftp(context, params, test)
                if params["params"]["condensed"]:
                    logInfo(context, params, "FTPcomplete")
                else:
                    logInfo(context, params, ftp_results)
                    pass
                if test == "download":
                    updateExecution(context, params, downloadTE, "PASSED",
                                    comments=[ftp_results])
                else:
                    updateExecution(context, params, uploadTE, "PASSED",
                                    comments=[ftp_results])
                    pass

                # Stop the metrics gathering so the log files stop getting data
                stopMetricsGathering(context, params)

                # Grab the metrics and attach to the run.
                ue_metrics,gnb_metrics = getMetrics(context, params)
                addAttachment(context, params,
                              "ue-metrics (%s) %s" % (test, it_text), ue_metrics)
                addAttachment(context, params,
                              "cudu-metrics (%s) %s" % (test, it_text), gnb_metrics)

                #
                # Dig out stats
                #
                results = condition["results"][test]

                # The curl output includes -w metrics.
                ftp_metrics = parseCurlResults(ftp_results)

                for key,val in ftp_metrics.items():
                    # Better name for this metric
                    if key == "Throughput":
                        key = "App UL/DL throughput (Mbps)"
                    elif key == "PageLoadTime":
                        key = "UL/DL Time (sec)"
                        pass

                    if not key in results:
                        results[key] = []
                        pass
                    results[key].append(val)
                    pass

                # Add some of the UE metrics that are required.
                parseUEMetrics(ue_metrics, results)
            
                # Add some GNB metrics.
                parseGNBMetrics(gnb_metrics, results)
                pass
            
            detachUE(context, params, detachTE)

            iterations = iterations - 1
            count = count + 1
            pass
        #
        # Compute mean and std deviation for each
        #
        stats = {
            "upload"   : {},
            "download" : {},
        }
        for test in ["upload", "download"]:
            for key,values in condition["results"][test].items():
                stats[test][key] = {
                    "mean"  : statistics.mean(values),
                    "stdev" : statistics.stdev(values),
                }
                pass
            pass
        condition["stats"] = stats
        pass

    pp_conditions = json.dumps(conditions, indent=4)
    if not params["params"]["condensed"]:
        logInfo(context, params, pp_conditions)
        pass
    addAttachment(context, params, "json-summary", pp_conditions)

    # Do not leave at non-zero
    setAttenuation(context, params, "bru1", 0)

    #
    # Use python tabulate module to format a table.
    #
    header = ["Test"]
    rows    = []

    # Init header and tests
    for condition in conditions:
        for test in ["upload", "download"]:
            description = condition["description"] + " (" + test + ")"
            attenuation = condition["attenuation"]
            header.append("%s\nAtten: %d dB (%d)"%
                          (description, attenuation + ATTENUATION_CONSTANT,
                           attenuation))
            pass
        
        for key,val in conditions[0]["results"]["upload"].items():
            rows.append([key + "\n" + "mean/stdev"])
            pass
        pass

    for condition in conditions:
        for test in ["upload", "download"]:
            index = 0
            for key,stats in condition["stats"][test].items():
                if key.startswith("DNS"):
                    rows[index].append("N/A")
                else:
                    rows[index].append("%.4f\n%.4f" % (stats["mean"], stats["stdev"]))
                    pass
                index = index + 1
                pass
            pass
        pass

    table = tabulate(rows, header, tablefmt="grid")
    if not params["params"]["condensed"]:
        logInfo(context, params, table)
        pass
    addAttachment(context, params, "Summary Results", table)
    
    saveLog(context, params);
    return params

#
# Peak upload and download
#
@op(required_resource_keys={"sshClient", "kiwiTCMS"})
def testPeakUpload(context, params: dict):
    testPeakUploadDownload(context, params, "upload")
    return params

@op(required_resource_keys={"sshClient", "kiwiTCMS"})
def testPeakDownload(context, params: dict):
    testPeakUploadDownload(context, params, "download")
    return params

def testPeakUploadDownload(context, params, direction):
    kiwiTCMS  = context.resources.kiwiTCMS
    sshClient = context.resources.sshClient
    results = {
        "UDP" : {},
        "TCP" : {},
    }

    if False:
        return params

    clearLog(context, params)

    # Create TestRun and associated TestCases.
    kiwiTCMS.createTestRun(params, "Peak " + direction + " (TIFG E2E X.X.X)")

    # Speed things along
    iterations = 6
    if params["params"]["debug"]:
        iterations = 2
        pass

    for proto in ["UDP", "TCP"]:
        count = 0
        while count < iterations:
            it_text  = "(%s: iteration %d)" % (proto, count+1)
            testTE   = kiwiTCMS.addTestCase(params, direction + " " + it_text)

            logInfo(context, params, direction + " " + it_text)

            # Reset the GNB logs
            restartGNB(context, params)
    
            attachUE(context, params, "bru1")

            # Start gathering metrics.
            startMetricsGathering(context, params)

            logInfo(context, params, "Running iperf")
            iperf_results = runIperf(context, params, timeout=30,
                                    direction=direction, proto=proto)

            if params["params"]["condensed"]:
                logInfo(context, params, "iperf complete")
            else:
                logInfo(context, params, iperf_results)
                pass
            updateExecution(context, params, testTE, "PASSED",
                            comments=[iperf_results])

            # Stop the metrics gathering so the log files stop getting data
            stopMetricsGathering(context, params)

            # Grab the metrics and attach to the run.
            ue_metrics,gnb_metrics = getMetrics(context, params)
            addAttachment(context, params, "ue-metrics " + it_text, ue_metrics)
            addAttachment(context, params, "cudu-metrics " + it_text, gnb_metrics)

            detachUE(context, params)

            #
            # Dig out stats
            #
            parseIperfResults(iperf_results, results[proto], direction)

            # Add some of the UE metrics that are required.
            parseUEMetrics(ue_metrics, results[proto])
            
            # Add some GNB metrics.
            parseGNBMetrics(gnb_metrics, results[proto])
            
            count = count + 1
            pass
        pass

    # Do not leave at non-zero
    setAttenuation(context, params, "bru1", 0)

    #
    # Compute mean and std deviation for each
    #
    stats = {
        "UDP" : {},
        "TCP" : {},
    }
    for proto in ["UDP", "TCP"]:
        for key,values in results[proto].items():
            stats[proto][key] = {
                "mean"  : statistics.mean(values),
                "stdev" : statistics.stdev(values),
            }
            pass
        pass

    tmp = {
        "results" : results,
        "stats"   : stats,
    }
    pp_results = json.dumps(tmp, indent=4)
    if not params["params"]["condensed"]:
        logInfo(context, params, pp_results)
        pass
    addAttachment(context, params, "json-summary", pp_results)

    #
    # Use python tabulate module to format a table.
    #
    header = ["Test", "UDP", "TCP"]
    rows    = []

    # Metric names in the first column of every row
    for key,val in stats["UDP"].items():
        rows.append([key + "\n" + "mean/stdev"])
        pass

    for proto in ["UDP", "TCP"]:
        index = 0
        for key,values in stats[proto].items():
            if key.startswith("DNS"):
                rows[index].append("N/A")
            else:
                rows[index].append("%.4f\n%.4f" % (values["mean"], values["stdev"]))
                pass
            index = index + 1
            pass
        pass

    table = tabulate(rows, header, tablefmt="grid")
    if not params["params"]["condensed"]:
        logInfo(context, params, table)
        pass
    addAttachment(context, params, "Summary Results", table)
    
    saveLog(context, params);
    return params

@job(
    config = {
        "ops" : {
            "start" : {
                "config" : {
                    # The defaults can be overridden in the WEB UI.
                    "params" : input_params
                }
            }
        }
    },
    resource_defs = resource_defs
)
def startTests():
    params = start()
#    start, started = CheckSutStarted(params)
#    r1 = StartSut(params, start)
#    r2 = SutStarted(started)
    #Merge, for the graph to look nice.
#    merged = SutReady([r1, r2])
    params = initTests(params)
    #params = testRegistration(params)
    #params = testHandover(params)
    #params = testWebBrowsing(params)
    #params = testFTP(params)
    params = testPeakDownload(params)
    params = testPeakUpload(params)
    params = cleanup(params)
    pass

