import tempfile

# Place holders
EMULAB_UID        = "leebee"
EMULAB_PID        = "PowderTeam"

# The workflow will create this import file so we have the actual user/project
try:
    from . import workflowDefs
    EMULAB_UID = workflowDefs.EMULAB_UID
    EMULAB_PID = workflowDefs.EMULAB_PID
except ImportError:
    pass    

input_params = {
    'proj' : EMULAB_PID,
    'username' : EMULAB_UID,
    'sut_profile_name' : "PowderTeam,srs-ho-test",
    'sut_experiment_name' : "srs-ho-test",
    'sut_paramset' : None,
    'skip_tcms' : True,
    'debug' : False,
    'condensed': False,
}
