from dagster import Definitions
from . import resources
from . import startTests

defs = Definitions(
    jobs=[startTests.startTests],
    resources=resources.resource_defs
)
